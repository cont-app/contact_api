"use strict";

module.exports = function(Contact) {
  Contact.getContact = async function(id) {
    let res = await Contact.find({
      where: {
        parent_id: id
      }
    });
    return res;
  };

  Contact.remoteMethod("getContact", {
    accepts: {
      arg: "id",
      type: "string"
    },
    returns: {
      arg: "contact",
      type: "array"
    },
    http: {
      verb: "get",
      path: "/getContact"
    }
  });

  Contact.favoriteContact = async function(id, boo) {
    let favUpd = await Contact.update(
      {
        id: id
      },
      {
        favorite: !boo
      }
    );
    return favUpd;
  };

  Contact.remoteMethod("favoriteContact", {
    accepts: [
      {
        arg: "id",
        type: "string"
      },
      {
        arg: "boo",
        type: "Boolean"
      }
    ],
    returns: {
      arg: "data",
      type: "string"
    },
    http: {
      verb: "get",
      path: "/favoriteContact"
    }
  });

  Contact.search = async (id, term) => {
    let data = await Contact.find({
      where: {
        parent_id: id,
        name: { regexp: `^.*${term}.*$` }
      }
    });

    return data;
  };

  Contact.remoteMethod("search", {
    accepts: [
      {
        arg: "id",
        type: "string"
      },
      {
        arg: "term",
        type: "string"
      }
    ],
    returns: {
      arg: "contacts",
      type: "array"
    },
    http: {
      verb: "get",
      path: "/search"
    }
  });
};
