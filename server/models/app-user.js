"use strict";


module.exports = function (Appuser) {

  Appuser.getMyInfo = async id => {
    let data = Appuser.find({
      where: {
        id: id
      }
    });
    return data;
  };

  Appuser.remoteMethod("getMyInfo", {
    accepts: {
      arg: "id",
      type: "string"
    },
    returns: {
      arg: "userData",
      type: "Object"
    },
    http: {
      verb: "get",
      path: "/getMyInfo"
    }
  });
};
